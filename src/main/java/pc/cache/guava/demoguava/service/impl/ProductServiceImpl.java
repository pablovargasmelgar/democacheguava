package pc.cache.guava.demoguava.service.impl;

import org.springframework.stereotype.Service;

import pc.cache.guava.demoguava.model.Product;
import pc.cache.guava.demoguava.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Override
	public Product getProductById(Long id) {
		 try{
		        
	        System.out.println("sleep por 3 segundos... para simular llamada a backend.");
		    Thread.sleep(1000*3);
		}catch (Exception e){
		    e.printStackTrace();
		}
		
		return new Product(id,"Producto " + id , "Q 10.00");
	}

}
