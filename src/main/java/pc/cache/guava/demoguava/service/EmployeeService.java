package pc.cache.guava.demoguava.service;

import pc.cache.guava.demoguava.model.Employee;

public interface EmployeeService {
	
	Employee getEmployeeById(Long id);

}
