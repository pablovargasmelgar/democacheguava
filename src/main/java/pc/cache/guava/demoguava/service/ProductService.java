package pc.cache.guava.demoguava.service;

import pc.cache.guava.demoguava.model.Product;

public interface ProductService {
	
	Product getProductById(Long id);

}
