package pc.cache.guava.demoguava.service.impl;

import org.springframework.stereotype.Service;

import pc.cache.guava.demoguava.model.Employee;
import pc.cache.guava.demoguava.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee getEmployeeById(Long id) {

		try{
            //Simula obtener el registro desde servicio de backend con un sleep en el thread
            System.out.println("sleep de 3 segundos... para simular llamada a backend.");
            Thread.sleep(1000*3);
        }catch (Exception e){
            e.printStackTrace();
        }

        //Registro dummy
        return new Employee(id,"Pablo Vargas " + id ,"Software Developer");
	}

}
