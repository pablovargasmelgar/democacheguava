package pc.cache.guava.demoguava.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import pc.cache.guava.demoguava.datacache.CacheStore;
import pc.cache.guava.demoguava.model.Product;
import pc.cache.guava.demoguava.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
    ProductService productService;

    @Autowired
    CacheStore<String> productNameCache;

    @GetMapping("/product/{id}")
    public String searchProductNameById(@PathVariable Long id) {
        System.out.println("Buscando el nombre del producto por Id  : " + id);

      //Busca el registro del empleado en cache y si lo encuentra lo devuelve desde la misma
        String cachedProductName = productNameCache.get(id);
        if(cachedProductName != null) {
            System.out.println("Producto encontrado en cache : " + cachedProductName);
            return cachedProductName;
        }

        //En caso el registro no este en cache
        //lo obtiene desde "backend"
        Product productFromBackendService = productService.getProductById(id);

        //Extrae el nombre del producto y lo agrega a la cache
        productNameCache.add(id, productFromBackendService.getName());
        return productFromBackendService.getName();
    }
    
}
