package pc.cache.guava.demoguava.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import pc.cache.guava.demoguava.datacache.CacheStore;
import pc.cache.guava.demoguava.model.Employee;
import pc.cache.guava.demoguava.service.EmployeeService;

@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
    CacheStore<Employee> employeeCache;
	
	
	 @GetMapping("/employee/{id}")
	    public Employee searchEmployeeById(@PathVariable Long id) {
	        System.out.println("Buscando empleado por Id  : " + id);

	        //Busca el registro del empleado en cache y si lo encuentra lo devuelve desde la misma
	        Employee cachedEmpRecord = employeeCache.get(id);
	        if(cachedEmpRecord != null) {
	            System.out.println("Registro del empleado encontrado en cache: " + cachedEmpRecord.getName());
	            return cachedEmpRecord;
	        }

	        //En caso el registro no este en cache
	        //lo obtiene desde "backend"
	        Employee EmpRecordFromBackendService = employeeService.getEmployeeById(id);

	        //Guarda el registro de empleado en cache
	        employeeCache.add(id, EmpRecordFromBackendService);

	        return EmpRecordFromBackendService;
	    }

}
