package pc.cache.guava.demoguava.datacache;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class CacheStore<T> {
	
	private Cache<Long, T> cache;

    //Constructor para hacerle build a CacheStore
    public CacheStore(int expiryDuration, TimeUnit timeUnit) {
        cache = CacheBuilder.newBuilder()
                .expireAfterWrite(expiryDuration, timeUnit)
                .concurrencyLevel(Runtime.getRuntime().availableProcessors())
                .build();
    }

    //Metodo para obtener registros almacenado previamente usando la llave indicada
    public T get(Long key) {
        return cache.getIfPresent(key);
    }

    //Metodo para agregar un nuevo registro en Cache con la llave indicada
    public void add(Long key, T value) {
        if(key != null && value != null) {
            cache.put(key, value);
            System.out.println("Registro almacenado en "
                    + value.getClass().getSimpleName()
                    + " cache con la llave = " + key);
        }
    }

}
