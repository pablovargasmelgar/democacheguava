package pc.cache.guava.demoguava.config;

import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import pc.cache.guava.demoguava.datacache.CacheStore;
import pc.cache.guava.demoguava.model.Employee;

@Configuration
public class CacheStoreBeans {
	
	@Bean
	public CacheStore<Employee> employeeCache(){
		//Crea la cache store para la cache de empleado, usando como unidad de tiempo segundos
		return new CacheStore<Employee>(120, TimeUnit.SECONDS);
	}
	
	@Bean
	public CacheStore<String> productNameCache(){
		//Crea la cache store para la cache de empleado, usando como unidad de tiempo horas
		return new CacheStore<String>(1, TimeUnit.HOURS);
	}
	

}
