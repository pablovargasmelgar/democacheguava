package pc.cache.guava.demoguava.model;

public class Product {
	
	Long id;
	String name;
	String price;
	
	public Product() {
		
	}
	
	public Product(Long id, String name, String price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}


	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPrice() {
		return price;
	}
	
}