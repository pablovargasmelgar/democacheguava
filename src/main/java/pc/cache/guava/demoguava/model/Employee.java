package pc.cache.guava.demoguava.model;

public class Employee {
	
	Long id;
	String name;
	String role;
	
	public Employee() {
	
	}
	
	public Employee(Long id, String name, String role) {
		this.id = id;
		this.name = name;
		this.role = role;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getRole() {
		return role;
	}
	
}